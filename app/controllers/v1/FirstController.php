<?php

namespace ModuleControllers\v1;

// the v1 controller for the skeleton module.
// every public method in this class is callable using /api/v1/skeleton/method-name (no camelcase)
// some default methods are used to use a different setup then the default in the AbstractBasicApiController.
// if an default public method is not allowed just add an rule like:
// public function methodName() {}
// on 1 single rule and the default method (if exists) is overwritten to be disabled.
class FirstController extends \AbstractBasicApiController {

    public function test() {
        echo 'its work!!!';

        exit;
    }

//	protected function getModelName() {
//		return \Models\Data::class;
//	}

	// add a validation before saving.
	protected function validateSave() {
		return true;
	}

	// only use this if sql where fields are always requres
	// the $all param is auto set to true if the get-all method is invoked so you can use different rules.
//	protected function whereFieldsRequired($all = true) {
//		if(!$all) {
//			return [
//				'id'    => 'id',
//			];
//		}
//
//		return [];
//	}
//
//	protected function filterFields() {
//		return [
//			'%name%'  => 'name',
//			'%email%' => 'email',
//		];
//	}
//
//	protected function sortFields() {
//		return [
//			'name'  => 'name',
//			'email' => 'email',
//		];
//	}


	// returns one skeleton model
	// public function get() {}

	// returns all id's (but disabled as example)
	// public function getIds() {}

	// returns all records (but optional a filter is applied)
	// public function getAll() {}

	// returns all records (but optional a filter is applied) in pages.
	// public function getPaginated() {}

	// returns a random record
	// public function getRandom() {}

	// create new record
	// public function create() {}

	// update existing record using id.
	// public function update() {}

	// removes a record using id.
	// public function delete() {}

	// returns the date the last modification is made
	// public function lastModified() {}

	// return the total of records available (optional a filter is applied)
	// public function count() {}

	// return the total sum of given (numeric) fields of records available (optional a filter is applied) (disabled for example)
	public function sum() {}

}
