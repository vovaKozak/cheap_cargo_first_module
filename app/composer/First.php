<?php

namespace CheapCargo\Setup;

abstract class First extends \CheapCargo\Setup\AbstractSetup {

	// default methods for running the installation of the module.
	// if custom code or checks needs to be done you can add the corresponding methods
	// in this class wich will run EVEREY TIME composer install/update/dumpautoload is executed.
	public static function install() {
		self::running(__CLASS__);

		parent::installConfigs(__CLASS__);
	}
}
