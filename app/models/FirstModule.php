<?php

namespace Models;

class FirstModule extends \Models\AbstractData {

	// This is only used if the default model/class name is not the same as the table name.
	// For example when the table name has a date suffix like skeleton_2020_03 (current date).
	// public function getSource() {
	// 	return parent::getSource().\Components\Date::now()->toString('_Y_m');
	// }

	// This is only used if you need to do something before the actual save action is triggered.
	// public function save($data = NULL, $whiteList = NULL) {
	// 	$tableName = $this->getSource();
	// 	if (!$this->getWriteConnection()->tableExists($tableName)) {
	// 		$this->createTable($tableName);
	// 	}
	// 	parent::save();
	// }

	// default query builder
	public static function getBasicBuilder() {
		$builder = static::builder()
			->columns('*')
			->from(self::class)
			->where('client_id = :client_id:', [
				'client_id' => static::getClientId(),
			])
			->orderBy('id');
		return $builder;
	}

	// The validation of this model before it's been saved to db
	public function validation() {
		// throw new \Exceptions\RouteException(\Exceptions\RouteException::INSUFFICIENT_SCOPES);

		$validator = new \Phalcon\Validation();

		$validator->add(['name'], new \Phalcon\Validation\Validator\PresenceOf([
			'code' => \Exceptions\DataException::NAME_REQUIRED
		]));

		$validator->add(['email'], new \Phalcon\Validation\Validator\PresenceOf([
			'code' => \Exceptions\DataException::EMAIL_REQUIRED
		]));
		$validator->add(['email'], new \Phalcon\Validation\Validator\Email([
			'code' => \Exceptions\DataException::INVALID_EMAIL
		]));

		return $this->validate($validator);
	}

	// when table creation is on the fly for example when table name has a date here the create table data is stored
	// private function createTable($name) {
	// 	$this->getWriteConnection()->createTable($name, null, [
	// 		'columns' => [
	// 			new \Phalcon\Db\Column('id', [
	// 				'type' => \Phalcon\Db\Column::TYPE_CHAR,
	// 				'size' => 36,
	// 				'notNull' => true,
	// 				'primary' => true,
	// 			]),
	// 			new \Phalcon\Db\Column('name', [
	// 				'type' => \Phalcon\Db\Column::TYPE_VARCHAR,
	// 				'size' => 255,
	// 			]),
	// 			new \Phalcon\Db\Column('email', [
	// 				'type' => \Phalcon\Db\Column::TYPE_VARCHAR,
	// 				'size' => 255,
	// 			]),
	//
	// 			// everey table should end with this 4 columns.
	// 			new \Phalcon\Db\Column('created_on', [
	// 				'type' => \Phalcon\Db\Column::TYPE_DATETIME,
	// 				]),
	// 			new \Phalcon\Db\Column('created_by', [
	// 				'type' => \Phalcon\Db\Column::TYPE_CHAR,
	// 				'size' => 36,
	// 			]),
	// 			new \Phalcon\Db\Column('modified_on', [
	// 				'type' => \Phalcon\Db\Column::TYPE_DATETIME,
	// 			]),
	// 			new \Phalcon\Db\Column('modified_by', [
	// 				'type' => \Phalcon\Db\Column::TYPE_CHAR,
	// 				'size' => 36,
	// 			]),
	// 		]
	// 	]);
	// }

}
