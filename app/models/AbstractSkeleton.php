<?php

namespace Models;

// general Models methods
// the name of the methods are clear.
// all models should have there methods so an abstract class is an easy way to do this.
abstract class AbstractSkeleton extends \AbstractBasicApi {

	public function afterValidation() {

	}

	public function beforeValidationOnCreate() {
		$this->id = \Components\Guid::newGuid();
		$this->client_id = static::getClientId();
		$this->created_by = static::getClientId();
	}

	public function beforeValidationOnUpdate() {
		$this->modified_by = static::getClientId();
	}

	protected static function getClientId() {
		$api = \Phalcon\DI::getDefault()->getShared('api');

		return $api->getAccessToken()->getClientId();
	}

}
