<?php

namespace Modules\first;

class Module extends \Abstracts\AbstractModule {

	public function registerAutoloaders(\Phalcon\Loader $loader) {

		$loader->registerNamespaces([
			'ModuleControllers' => $this->path.'/controllers',
//			'Exceptions'        => $this->path.'/exceptions',
//			'Models'            => $this->path.'/models',
		], true);
	}

	public function registerServices(\Phalcon\DiInterface $di) {
	}

	public function registerRoutes(\Phalcon\Mvc\Micro $app) {

		$app->map('/api/v{version:[0-9.]+}/{control}[/]?{method}', function($version, $controller, $method = null, $args = []) use ($app) {

			$app->dispatcher->setNamespaceName('ModuleControllers\\v'.$version);
			$app->dispatcher->setControllerName($controller);
			$app->dispatcher->setActionSuffix('');
			$app->dispatcher->setActionName(lcfirst(\Phalcon\Text::camelize($method)));
			$app->dispatcher->setParams($args);
			$app->dispatcher->dispatch();

			$value = $app->dispatcher->getReturnedValue();

			if ($value === null) {
				throw new \Exceptions\RouteException(\Exceptions\RouteException::INVALID_ACTION);
			}

			$app->response->setResult($value, is_array($value));
		});
	}

	public function registerConfig() {
		$config = new \Phalcon\Config\Adapter\Json($this->configPath.'/config/config.json');

		$request = new \Phalcon\Http\Request();
		$override = [
			'request' => $request->getHeader('X-Show-Request'),
			'errors'  => $request->getHeader('X-Show-Errors'),
			'status'  => $request->getHeader('X-Show-Status'),
		];

		foreach ($override as $key => $value) {
			if (!empty($value) && $value == "true") {
				if (!isset($config->application)) {
					$config->application = new \Phalcon\Config();
				}
				if (!isset($config->application->removeEmpty)) {
					$config->application->removeEmpty = new \Phalcon\Config();
				}
				$config->application->removeEmpty->$key = false;
			}
		}

		return $config;
	}
}
