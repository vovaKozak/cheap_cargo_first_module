<?php

namespace Exceptions;

class FirstException extends \Exceptions\AbstractException {

	const NAME_REQUIRED  = -1;
	const EMAIL_REQUIRED = -2;
	const INVALID_EMAIL  = -3;

	protected static function setMessages() {
		return [
			self::NAME_REQUIRED  => parent::_('Field name is required'),
			self::EMAIL_REQUIRED => parent::_('Field email is required'),
			self::INVALID_EMAIL  => parent::_('The given email is invalid'),
		];
	}

}
