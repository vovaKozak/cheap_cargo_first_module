# It is just test...

## Setup

This module is just en example backend module to be used as an empty template for a quick setup of a new backend module.

The folowing steps are used to setup this module (also see git commits for file examples):

1. Create a new git repository (with development, test and master branches).
* Setup a /composer.json
* Create the directory structure *
* Create a /Module.php
* Create Models
* Create Exceptions
* Create Controllers
* Create example config **
* Add composer setup file
* Add 3 sql assets (auto increment for the order to run)

The modules is now ready to use.
Next step is installing this module into backend.

1. Create database
2. Add user credentials and rights (same as other backend modules within this backend)
3. Import 3 sql assets (incremental)
4. Add module to main composer.json ***
5. Update the new config file in /modules/skeleton/config/config.json (if necessary)
6. Check if the existing routes in the route_cache table are valid ****
7. Add module to routes in admin and press push (all) route(s) button.
8. Check if module is working by executing GET http://backend.test/api/v1/skeleton/test

## After installation
When installation is finished, routes are received from masterrouter and test method gives no errors the module is ready to use.
Create new Controllers and Models (or modify the existing ones) en  build a great module ;)


## Explanations
*) directory structure like:

	- app
		- assets
			- paw
			- sql
		- composer
		- config
		- controllers
			- v1
		- exceptions
		- models

**) config is required to have following fields:

	{
	    "database" : {
	        "connection" : {
	            "dbname" : "name_of_db"
	        },
	        "tables" : {
	            "ModelName" : "table_name"
	        },
	    },
	    "application" : {
	        "debug": true,
	        "accesstokencache": false,
	        "prettyPrint": true,
	        "removeEmpty": {
	            "request": false,
	            "errors": false,
	            "status": false
	        }
	    }
	}

it is allowed to add custom config params.
if the module needs a connection to an other backend module this should be added:

    "routes": {
        "configName": "routercode"
    },

If the module had extra scopes for more security add them here:

    "scopes": {
        "optionalExtraScope": "optionalExtraScope"
    },

***) add repository to composer.json like:

	{
		"type": "git",
		"url": "git@gitlab.com:cheapcargo-backend-mods/mod-skeleton.git"
	}

the to add the module, run `composer require cheapcargo/backend-skeleton-mod:dev-development`

****) There are 2 tables for the resource server in the db.
access_token_cache which should be empty and route_cache with 2 records.
One for the auth service and one for the masterrouter service.
Update the domain names and protocols.